/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.eclipse.openk.contactbasedata.ContactBaseDataApplication;
import org.eclipse.openk.contactbasedata.service.CommunicationService;
import org.eclipse.openk.contactbasedata.support.MockDataHelper;
import org.eclipse.openk.contactbasedata.viewmodel.CommunicationDto;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = ContactBaseDataApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
class CommunicationControllerTest {

    @MockBean
    private CommunicationService communicationService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void shouldReturnCommunicationsForAContact() throws Exception {
        List<CommunicationDto> listDtos = MockDataHelper.mockCommunicationDtoList();

        when(communicationService.getCommunicationsByContactUuid(any(UUID.class))).thenReturn(listDtos);

        mockMvc.perform(get("/contacts/{uuid}/communications", UUID.randomUUID()))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    void shouldReturnSingleCommunication() throws Exception {
        CommunicationDto communicationDto = MockDataHelper.mockCommunicationDto();

        when(communicationService.getCommunication(any(UUID.class), any(UUID.class))).thenReturn(communicationDto);

        mockMvc.perform(get("/contacts/a689622e-3c2b-11ea-b77f-2e728ce88125/communications/a68964f4-3c2b-11ea-b77f-2e728ce88125"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }


    @Test
    void shouldInsertCommunication() throws Exception {
        CommunicationDto communicationDto = MockDataHelper.mockCommunicationDto();
        communicationDto.setUuid(UUID.fromString("498a1874-3c2c-11ea-b77f-2e728ce88125"));

        communicationDto.setContactUuid(UUID.fromString("64e35f5e-3c2c-11ea-b77f-2e728ce88125"));
        communicationDto.setCommunicationData("01778927384");
        communicationDto.setCommunicationTypeType("MOB");
        communicationDto.setCommunicationTypeDescription("Mobil");
        communicationDto.setCommunicationTypeUuid(UUID.fromString("70160958-3c2c-11ea-b77f-2e728ce88125"));

        when(communicationService.insertCommunication(any(UUID.class), any(CommunicationDto.class)))
                .thenReturn(communicationDto);

        mockMvc.perform(post("/contacts/{contactUuid}/communications", communicationDto.getContactUuid())
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(communicationDto)))
                .andExpect(jsonPath("$.contactId", Matchers.is("64e35f5e-3c2c-11ea-b77f-2e728ce88125")))
                .andExpect(jsonPath("$.communicationData", Matchers.is("01778927384" )))
                .andExpect(jsonPath("$.communicationTypeType", Matchers.is("MOB" )))
                .andExpect(jsonPath("$.communicationTypeDescription", Matchers.is("Mobil" )))
                .andExpect(jsonPath("$.communicationTypeId", Matchers.is("70160958-3c2c-11ea-b77f-2e728ce88125" )));
    }


    @Test
    void shouldUpdateCommunication() throws Exception {

        CommunicationDto communicationDto = MockDataHelper.mockCommunicationDto();
        when( communicationService.updateCommunication(any(UUID.class), any(CommunicationDto.class))).thenReturn(communicationDto);

        mockMvc.perform(put("/contacts/{contactUuid}/communications/{communicationUuid}", communicationDto.getContactUuid(), communicationDto.getUuid())
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(communicationDto)))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    void shouldNotUpdateCommunication() throws Exception {

        CommunicationDto communicationDto = MockDataHelper.mockCommunicationDto();
        when( communicationService.updateCommunication(any(UUID.class), any(CommunicationDto.class))).thenReturn(communicationDto);

        mockMvc.perform(put("/contacts/{contactUuid}/communications/{communicationUuid}", communicationDto.getContactUuid(), UUID.randomUUID().toString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(communicationDto)))
                .andExpect(status().isBadRequest());
    }


    @Test
    void shouldNotUpdateCommunicationBecauseOfInvalidArgs() throws Exception {

        CommunicationDto communicationDto = MockDataHelper.mockCommunicationDto();
        communicationDto.setCommunicationTypeUuid(null);
        communicationDto.setCommunicationTypeType(null);
        when( communicationService.updateCommunication(any(UUID.class), any(CommunicationDto.class))).thenReturn(communicationDto);

        mockMvc.perform(put("/contacts/{contactUuid}/communications/{communicationUuid}", communicationDto.getContactUuid(), UUID.randomUUID().toString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(communicationDto)))
                .andExpect(status().isBadRequest());
    }
    @Test
    void shouldDeleteCommunication() throws Exception {
        mockMvc.perform(delete("/contacts/{contactUuid}/communications/{communicationUuid}", UUID.randomUUID(), UUID.randomUUID())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful());
    }


}