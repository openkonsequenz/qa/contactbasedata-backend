package org.eclipse.openk.contactbasedata.service.util;

import org.eclipse.openk.contactbasedata.ContactBaseDataApplication;
import org.eclipse.openk.contactbasedata.support.MockDataHelper;
import org.eclipse.openk.contactbasedata.viewmodel.LdapUser;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = ContactBaseDataApplication.class)
@ActiveProfiles("test")
class LdapUserAttributesMapperTest {

    @Autowired
    LdapUserAttributesMapper ldapUserAttributesMapper;

    @Test
    void shouldMapFromAttributesCorrectly() throws NamingException {
        Attributes attributes = Mockito.mock(Attributes.class);

        String[] fields = {"uid", "cn", "sn", "givenname", "title", "mail", "department", "phone"};

        Arrays.stream(fields).forEach( x -> {
                    Attribute attr = MockDataHelper.mockLdapAttribute('_' + x + "_");
                    when(attributes.get(x)).thenReturn(attr);
                });

        LdapUser ldapUser = ldapUserAttributesMapper.mapFromAttributes(attributes);
        assertEquals("_uid_", ldapUser.getUid());
        assertEquals("_cn_", ldapUser.getFullName());
        assertEquals("_sn_", ldapUser.getLastName());
        assertEquals("_givenname_", ldapUser.getFirstName());
        assertEquals("_title_", ldapUser.getTitle());
        assertEquals("_mail_", ldapUser.getMail());
        assertEquals("_department_", ldapUser.getDepartment());
        assertEquals("_phone_", ldapUser.getTelephoneNumber());

    }

    @Test
    void shouldMapFromAttributesEmpty() throws NamingException {
        Attributes attributes = Mockito.mock(Attributes.class);

        String[] fields = {"uid", "cn", "sn", "givenname", "title", "mail", "department", "phone"};

        Arrays.stream(fields).forEach( x -> {
            when(attributes.get(x)).thenReturn(null);
        });

        LdapUser ldapUser = ldapUserAttributesMapper.mapFromAttributes(attributes);
        assertEquals(null, ldapUser.getUid());
        assertEquals(null, ldapUser.getFullName());
        assertEquals(null, ldapUser.getLastName());
        assertEquals(null, ldapUser.getFirstName());
        assertEquals(null, ldapUser.getTitle());
        assertEquals(null, ldapUser.getMail());
        assertEquals(null, ldapUser.getDepartment());
        assertEquals(null, ldapUser.getTelephoneNumber());

    }

}
