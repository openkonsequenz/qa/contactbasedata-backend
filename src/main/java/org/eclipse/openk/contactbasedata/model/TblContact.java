/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.model;

import lombok.Data;

import jakarta.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@Entity
public class TblContact {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,  generator = "tbl_contact_id_seq")
    @SequenceGenerator(name = "tbl_contact_id_seq", sequenceName = "tbl_contact_id_seq", allocationSize = 1)
    @Column(name = "id", updatable = false)
    private Long id;
    private UUID uuid;
    private String contactType;
    private String note;
    private Boolean anonymized;

    @OneToMany(cascade=CascadeType.ALL, mappedBy="tblContact", orphanRemoval = true)
    private List<TblAddress> addresses = new ArrayList<>();

    @OneToMany(cascade=CascadeType.ALL, mappedBy="tblContact", orphanRemoval = true)
    private List<TblCommunication> communications = new ArrayList<>();

    @OneToMany(cascade=CascadeType.ALL, mappedBy="tblContact", orphanRemoval = true)
    private List<TblAssignmentModulContact> assignments = new ArrayList<>();

}



