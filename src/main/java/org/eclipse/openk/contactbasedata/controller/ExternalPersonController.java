/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.contactbasedata.exceptions.BadRequestException;
import org.eclipse.openk.contactbasedata.service.ExternalPersonService;
import org.eclipse.openk.contactbasedata.viewmodel.ExternalPersonDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.Optional;
import java.util.UUID;

@Log4j2
@RestController
@RequestMapping("/external-persons")
public class ExternalPersonController {
    @Autowired
    private ExternalPersonService externalPersonService;

    @GetMapping("/{contactUuid}")
    @Secured({"ROLE_KON-READER", "ROLE_KON-WRITER", "ROLE_KON-ADMIN"})
    @Operation(summary = "Anzeigen einer externen Person")
    @ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Erfolgreich durchgeführt"),
                            @ApiResponse(responseCode = "404", description = "Person wurde nicht gefunden")})
    @ResponseStatus(HttpStatus.OK)
    public ExternalPersonDto getExternalPerson(@PathVariable UUID contactUuid) {
        return externalPersonService.findExternalPerson(contactUuid);
    }

    @GetMapping
    @Secured({"ROLE_KON-READER", "ROLE_KON-WRITER", "ROLE_KON-ADMIN"})
    @Operation(summary = "Anzeigen aller externen Personen", description = "Sortieren ist möglich. Externe Personen werden seitenweise geliefert.")
    @ResponseStatus(HttpStatus.OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Erfolgreich durchgeführt")})
    public Page<ExternalPersonDto> readExternalPersons(
            @RequestParam( "alsoShowAnonymous") Optional<Boolean> alsoShowAnonymous,
            @PageableDefault(sort = {"lastName"}, direction = Sort.Direction.ASC) Pageable pageable){
            return externalPersonService.findExternalPersons(alsoShowAnonymous.orElse(false), pageable);
    }

    @PostMapping
    @Secured({"ROLE_KON-ADMIN", "ROLE_KON-WRITER"})
    @Operation(summary = "Anlegen einer externen Person")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "externe Person erfolgreich angelegt"),
            @ApiResponse(responseCode = "500", description = "Konnte nicht durchgeführt werden")
    })
    public ResponseEntity<ExternalPersonDto> insertExternalPerson(
            @Validated @RequestBody ExternalPersonDto externalPersonDto) {
        ExternalPersonDto savedExternalPersonDto = externalPersonService.insertExternalPerson(externalPersonDto);
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequestUri()
                .path("/{uuid}")
                .buildAndExpand(savedExternalPersonDto.getContactUuid())
                .toUri();
        return ResponseEntity.created(location).body(savedExternalPersonDto);
    }

    @PutMapping("/{contactUuid}")
    @Secured({"ROLE_KON-ADMIN", "ROLE_KON-WRITER"})
    @Operation(summary = "Ändern einer externen Person")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Externe Person wurde aktualisiert"),
            @ApiResponse(responseCode = "400", description = "Ungültige Eingabe"),
            @ApiResponse(responseCode = "404", description = "Nicht gefunden")})
    public ResponseEntity updateExternalPerson(@PathVariable UUID contactUuid, @Validated @RequestBody ExternalPersonDto externalPersonDto) {

        if (!externalPersonDto.getContactUuid().equals(contactUuid)) {
            throw new BadRequestException("invalid.uuid.path.object");
        }

        externalPersonService.updateExternalPerson(externalPersonDto);
        return ResponseEntity.ok().build();
    }
}

