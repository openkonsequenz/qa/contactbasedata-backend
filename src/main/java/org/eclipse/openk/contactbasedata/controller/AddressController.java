/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.contactbasedata.exceptions.BadRequestException;
import org.eclipse.openk.contactbasedata.service.AddressService;
import org.eclipse.openk.contactbasedata.viewmodel.AddressDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.UUID;

@Log4j2
@RestController
@RequestMapping("contacts/")
public class AddressController {
    @Autowired
    private AddressService addressService;

    @GetMapping("/{contactUuid}/addresses")
    @Secured({"ROLE_KON-READER", "ROLE_KON-WRITER", "ROLE_KON-ADMIN"})
    @Operation(summary = "Anzeigen aller Adressen zu einem Kontakt")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "400", description = "Ungültige Anfrage."),
            @ApiResponse(responseCode = "200", description = "Suche durchgeführt")})
    @ResponseStatus(HttpStatus.OK)
    public List<AddressDto> getContactAddresses(@PathVariable UUID contactUuid) {
        return addressService.getAddressesByContactUuid(contactUuid);
    }

    @GetMapping("/{contactUuid}/addresses/{addressUuid}")
    @Secured({"ROLE_KON-READER", "ROLE_KON-WRITER", "ROLE_KON-ADMIN"})
    @Operation(summary = "Eine bestimmte Adresse eines bestimmten Kontakts anzeigen.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "404", description = "Kontaktadresse nicht gefunden."),
            @ApiResponse(responseCode = "400", description = "Ungültige Anfrage."),
            @ApiResponse(responseCode = "200", description = "Kontaktadresse erfolgreich gelesen.")})
    public AddressDto readAddress(
            @PathVariable UUID contactUuid,
            @PathVariable UUID addressUuid) {

        return addressService.getAddress(contactUuid, addressUuid);
    }


    @PostMapping("/{contactUuid}/addresses")
    @Secured({"ROLE_KON-WRITER", "ROLE_KON-ADMIN"})
    @Operation(summary = "Anlegen einer neuen Adresse")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Adresse erfolgreich angelegt"),
            @ApiResponse(responseCode = "500", description = "Konnte nicht durchgeführt werden")
    })
    public ResponseEntity<AddressDto> insertAddress(
            @PathVariable UUID contactUuid,
            @Validated @RequestBody AddressDto addressDto) {
        if (!contactUuid.equals(addressDto.getContactUuid())) {
            throw new BadRequestException("invalid.uuid.path.object");
        }
        AddressDto savedAddressDto = addressService.insertAddress(contactUuid, addressDto);
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequestUri()
                .path("/{uuid}")
                .buildAndExpand(savedAddressDto.getUuid())
                .toUri();
        return ResponseEntity.created(location).body(savedAddressDto);
    }


    @PutMapping("/{contactUuid}/addresses/{addressUuid}")
    @Secured({"ROLE_KON-WRITER", "ROLE_KON-ADMIN"})
    @Operation(summary = "Eine bestimmte Adresse eines bestimmten Kontakts bearbeiten.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "404", description = "Adresse nicht gefunden."),
            @ApiResponse(responseCode = "400", description = "Ungültige Anfrage."),
            @ApiResponse(responseCode = "200", description = "Adresse erfolgreich geändert.")})
    public ResponseEntity updateAddress(
            @PathVariable UUID contactUuid,
            @PathVariable UUID addressUuid,
            @Validated @RequestBody AddressDto addressDto) {
        if (!addressUuid.equals(addressDto.getUuid())) {
            throw new BadRequestException("invalid.uuid.path.object");
        }
        addressService.updateAddress(contactUuid, addressDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("{contactUuid}/addresses/{addressUuid}")
    @Secured({"ROLE_KON-WRITER", "ROLE_KON-ADMIN"})
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Eine bestimmte Adresse eines bestimmten Kontakts löschen")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Erfolgreich gelöscht"),
            @ApiResponse(responseCode = "400", description = "Ungültige Anfrage"),
            @ApiResponse(responseCode = "404", description = "Nicht gefunden")})
    public void deleteAddress(@PathVariable("contactUuid") UUID contactUuid,
                              @PathVariable("addressUuid") UUID addressUuid) {
        addressService.deleteAddress(contactUuid, addressUuid);
    }

}
