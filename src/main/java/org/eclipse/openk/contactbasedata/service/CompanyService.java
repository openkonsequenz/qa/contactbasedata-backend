/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.service;

import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.contactbasedata.constants.Constants;
import org.eclipse.openk.contactbasedata.exceptions.NotFoundException;
import org.eclipse.openk.contactbasedata.mapper.CompanyMapper;
import org.eclipse.openk.contactbasedata.mapper.ContactMapper;
import org.eclipse.openk.contactbasedata.mapper.ContactPersonMapper;
import org.eclipse.openk.contactbasedata.model.TblCommunication;
import org.eclipse.openk.contactbasedata.model.TblCompany;
import org.eclipse.openk.contactbasedata.model.TblContact;
import org.eclipse.openk.contactbasedata.model.TblContactPerson;
import org.eclipse.openk.contactbasedata.repository.*;
import org.eclipse.openk.contactbasedata.viewmodel.CompanyDto;
import org.eclipse.openk.contactbasedata.viewmodel.ContactPersonDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@Log4j2
@Service
public class CompanyService {
    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private ContactRepository contactRepository;

    @Autowired
    private ContactPersonRepository contactPersonRepository;

    @Autowired
    private SalutationRepository salutationRepository;

    @Autowired
    private PersonTypeRepository personTypeRepository;

    @Autowired
    private CompanyMapper companyMapper;

    @Autowired
    private ContactPersonMapper contactPersonMapper;

    @Autowired
    private ContactMapper contactMapper;

    @Autowired
    private BaseContactService baseContactService;

    public CompanyDto findCompany(UUID contactUuid) {
        return companyMapper.toCompanyDto(
                companyRepository.findByTblContactUuid(contactUuid).orElseThrow(NotFoundException::new)
        );
    }

    public Page<CompanyDto> findCompanies(Boolean showAlsoAnonymous, Pageable pageable) {
        return (Boolean.TRUE.equals(showAlsoAnonymous)
                ? companyRepository.findAll(pageable)
                : companyRepository.findByContact_anonymizedFalseOrContact_anonymizedIsNull(pageable))
                .map(companyMapper::toCompanyDto);

    }


    @Transactional
    public CompanyDto insertCompany(CompanyDto companyDto) {
        TblContact contactToSave = new TblContact();
        contactToSave.setUuid(UUID.randomUUID());
        contactToSave.setContactType(Constants.CONTACT_TYPE_COMPANY);

        TblCompany companyToSave = companyMapper.toTblCompany(companyDto);
        companyToSave.setContact(contactToSave);
        contactRepository.save(companyToSave.getContact());

        setFromCompanyDto( companyToSave, companyDto );

        // Then save dependent Model-Object
        return companyMapper.toCompanyDto(companyRepository.save(companyToSave));
    }

    @Transactional
    public CompanyDto updateCompany(CompanyDto companyDto){
        TblCompany companyUpdated;

        TblCompany existingCompany = companyRepository
                .findByTblContactUuid(companyDto.getContactUuid())
                .orElseThrow(() -> new NotFoundException("contact.uuid.not.existing"));

        existingCompany.setCompanyName(companyDto.getCompanyName());
        existingCompany.setCompanyType(companyDto.getCompanyType());
        existingCompany.setHrNumber(companyDto.getHrNumber());

        setFromCompanyDto( existingCompany, companyDto );
        companyUpdated = companyRepository.save(existingCompany);

        return companyMapper.toCompanyDto(companyUpdated);
    }

    public List<ContactPersonDto> findContactPersonsToCompany(UUID companyContactUuid, boolean alsoShowAnonymous) {
        TblCompany company = companyRepository.findByTblContactUuid( companyContactUuid )
                .orElseThrow( NotFoundException::new);
        List<TblContactPerson> tblContactPeople = company.getContactPersons();

        if( !alsoShowAnonymous ) {
            tblContactPeople = tblContactPeople.stream().filter( cp -> cp.getContact().getAnonymized() == null
                                                               || !cp.getContact().getAnonymized() )
                            .collect(Collectors.toList());
        }
        Map<UUID, TblContactPerson> tblContactPersonMap = tblContactPeople.stream()
                .collect( Collectors.toMap(cp -> cp.getContact().getUuid(), x -> x));


        List<ContactPersonDto> contactPersonDtos = tblContactPeople.stream()
                .map(contactPersonMapper::toContactPersonDto)
                .collect(Collectors.toList());
        // fill in the emails
        contactPersonDtos.stream().forEach( x -> mapEmail( x, tblContactPersonMap ));
        return  contactPersonDtos;
    }


    private void mapEmail( ContactPersonDto dto, Map<UUID, TblContactPerson> cpMap ) {
        TblCommunication tblComm = cpMap.get(dto.getContactUuid()).getContact()
                        .getCommunications().stream()
                        .filter(comm -> comm.getRefCommunicationType().isTypeEmail())
                        .findFirst().orElse(null);

        dto.setEmail( tblComm == null ? "" : tblComm.getCommunicationData() );
    }

    private void setFromCompanyDto( TblCompany destTblCompany, CompanyDto sourceDto ) {

        destTblCompany.getContact().setNote(sourceDto.getContactNote());
        destTblCompany.getContact().setAnonymized(sourceDto.getContactAnonymized());
    }
}
