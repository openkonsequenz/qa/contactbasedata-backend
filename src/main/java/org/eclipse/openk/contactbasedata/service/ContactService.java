/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.service;

import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.contactbasedata.constants.Constants;
import org.eclipse.openk.contactbasedata.exceptions.NotFoundException;
import org.eclipse.openk.contactbasedata.mapper.ContactMapper;
import org.eclipse.openk.contactbasedata.model.TblContact;
import org.eclipse.openk.contactbasedata.model.VwDetailedContact;
import org.eclipse.openk.contactbasedata.repository.ContactRepository;
import org.eclipse.openk.contactbasedata.repository.DetailedContactRepository;
import org.eclipse.openk.contactbasedata.service.util.SearchContactsFilterParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Date;
import java.util.UUID;

@Log4j2
@Service
public class ContactService {
    @Autowired
    private DetailedContactRepository detailedContactRepository;
    @Autowired
    private ContactRepository contactRepository;
    @Autowired
    private ContactMapper contactMapper;

    public TblContact findTblContact(UUID contactUuid ) {
        return contactRepository.findByUuid( contactUuid ).orElseThrow( () -> new NotFoundException(Constants.CONTACT_UUID_NOT_EXISTING));
    }

    public Page<VwDetailedContact> findDetailedContacts(
            SearchContactsFilterParams searchContactsFilterParams,
            Pageable pageable) {
            return detailedContactRepository.findByFilter(
                    searchContactsFilterParams.getContactType(),
                    searchContactsFilterParams.getPersonTypeUuid(),
                    searchContactsFilterParams.getSearchText() != null ? searchContactsFilterParams.getSearchText().toUpperCase() : null,
                    searchContactsFilterParams.getModuleName(),
                    searchContactsFilterParams.isWithoutModule(),
                    Date.from(Instant.now()),
                    searchContactsFilterParams.isExpiringDataInPast(),
                    searchContactsFilterParams.isDelLockExceeded(),
                    searchContactsFilterParams.isShowAnonymized(),
                    searchContactsFilterParams.isWithSyncError(),
                    pageable);
    }

    public VwDetailedContact findDetailedContactByUuid(UUID contactUuid){
        return detailedContactRepository
                .findByUuid(contactUuid)
                .orElseThrow(() -> new NotFoundException("contact.uuid.not.existing"));
    }

}
